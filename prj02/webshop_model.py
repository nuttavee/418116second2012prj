class Customer (object):
    def __init__(self,email,first_name,last_name):
        self._email = email
        self._first_name = first_name
        self._last_name = last_name
        self._orders = []
    @property
    def email (self):
        return self._email
    @email.setter
    def email (self,value):
        self._email = value
    @property
    def first_name (self):
        return self._first_name
    @first_name.setter
    def first_name (self,value):
        self._first_name = value
    @property
    def last_name (self):
        return self._last_name
    @last_name.setter
    def last_name (self,value):
        self._last_name = value
    @property
    def orders(self):
        return self._orders
    
class Product (object):
    def __init__(self, sku, name, description, price_ex_vat):
        self._sku = sku
        self._name = name
        self._description = description
        self._price_ex_vat = price_ex_vat
    @property
    def sku (self):
        return self._sku
    @sku.setter
    def sku (self,value):
        self._sku = value
    @property
    def name (self):
        return self._name
    @name.setter
    def name (self,value):
        self._name = value
    @property
    def description (self):
        return self._description
    @description.setter
    def description (self,value):
        self._description = value
    @property
    def price_ex_vat (self):
        return self._price_ex_vat
    @price_ex_vat.setter
    def price_ex_vat (self,value):
        self._price_ex_vat = value
class Order (object):
    def __init__ (self, order_code, customer, products):
        self._order_code = order_code
        self._customer = customer
        self._products = products
    @property
    def order_code (self):
        return self._order_code
    @order_code.setter
    def order_code (self,value):
        self._order_code = value
    @property
    def customer (self):
        return self._customer
    @customer.setter
    def customer (self,value):
        self._customer = value
    @property
    def products (self):
        return self._products
    @products.setter
    def products (self,value):
        self._products = value
class Products (object):
    def __init__ (self):
        self._all = []
    @property
    def all (self):
        return self._all
class Customers (object):
    def __init__(self):
        self._all = []
    @property
    def all (self):
        return self._all 
class Orders (object):
    def __init__(self):
        self._all = []
    @property
    def all (self):
        return self._all    