from webshop_model import Customer, Product, Order, Customers, Products, Orders

class Webshop:

    def __init__(self):
        self.customers = Customers()
        self.products = Products()
        self.orders = Orders()

    def get_total_customers(self):
        """ Return total number of customers
        
        @rtype: integer
        """
        return len(self.customers.all)
        
    def get_total_products(self):
        """ Return total number of products
        
        @rtype: integer
        """
        return len(self.products.all)

    def get_total_orders(self):
        """ Return total number of orders
        
        @rtype: integer
        """
        return len(self.orders.all)

    def add_customer(self, email, first_name, last_name):
        """ Add new customer if he/she does not exist (identified by email)
        Return the new customer if successes otherwise None
        
        @param email: email address
        @type email: unicode string
        @param first_name: first name
        @type first_name: unicode string
        @param last_name: last name
        @type last_name: unicode string
        
        @rtype: Customer
        """
        for i in self.customers.all:
            if email == i.email:
                return None
        self.customers.all.append(Customer(email,first_name,last_name))
        return Customer(email,first_name,last_name)
    def add_product(self, sku, name, description, price_ex_vat):
        """ Add new product if it does not exist (identified by sku)
        Return the new product if successes otherwise None
        
        @param sku: SKU
        @type sku: unicode string
        @param name: name
        @type name: unicode string
        @param description: description
        @type description: unicode string
        @param price_ex_vat: price excluding vat
        @type price_ex_vat: float
        
        @rtype: Product
        """
        for i in self.products.all:
            if sku == i.sku:
                return None
        self.products.all.append(Product(sku, name, description, price_ex_vat))
        return Product(sku, name, description, price_ex_vat)
    def add_order(self, order_code, customer, products):
        """ Add new order if it does not exist (identified by order_code)
        Return the new order if successes otherwise None
        
        @param order_code: unique code of each product
        @type order_code: unicode string
        @param customer: customer
        @type customer: Customer
        @param products: products
        @type products: list of Product
        
        @rtype: Order
        """     
        for i in self.orders.all:
            if order_code == i.order_code:
                return None
        customer.orders.append(Order(order_code,customer,products))    
        self.orders.all.append(Order(order_code,customer,products))
        return Order(order_code,customer,products)        
    
    def delete_customer(self, email):
        """ Delete the customer who has the given email
        Return True if successes otherwise False
        
        @param email: email address
        @type email: unicode string
        
        @rtype: boolean
        """
        for i in self.customers.all:
            if email == i.email:
                self.customers.all.remove(i)
                return True
        return False
            
    def delete_product(self, sku):
        """ Delete the product which has the given sku
        Return True if successes otherwise False

        @param sku: SKU
        @type sku: unicode string
        
        @rtype: boolean
        """
        for i in self.products.all:
            if sku == i.sku:
                self.products.all.remove(i)
                return True
        return False        
        
    def delete_order(self, order_code):
        """ Delete the order which has the given order_code
        Return True if successes otherwise False

        @param order_code: unique code of each product
        @type order: unicode string
        
        @rtype: boolean        
        """
        for i in self.orders.all:
            if order_code == i.order_code:
                self.orders.all.remove(i)
                return True
        return False                

    def get_products_in_price_range(self, min_price=None, max_price=None):
        """ Get products in the given range of price

        @rtype: list of Product
        """
        if max_price == None:
            max_price = float('inf')
        return filter(lambda x:min_price < x.price_ex_vat < max_price,self.products.all)    

    def get_best_customers(self):
        """ Return customers that have highest total price of orders

        @rtype: list of Customer
        """
        a = []
        b = []
        for i in self.orders.all:
            total = 0
            for j in i.products:
                total+=j.price_ex_vat
            a.append([total,i.customer])
        a.sort()
        a.reverse()
        for k in a:
            b.append(k[1])
        return b
    
    def get_orders_of_customer(self, email):
        """ Get orders of the given customer
        
        @param email: email address
        @type email: unicode string

        @rtype: list of Order
        """
        a = []
        for i in self.orders.all:
            if i.customer.email == email:
                a.append(i)
        return a
    
    def get_customers_who_ordered_product(self, sku):
        """ Get customers who ordered the given product
        
        @param sku: SKU
        @type sku: unicode string
        
        @rtype: list of Customer
        """        
        a=[]
        for i in self.orders.all:
            for x in i.products:
                if x.sku == sku:
                    a.append(i.customer)
        a.sort()
        return a
        
    def get_cheapest_products(self):
        """ Return the cheapest products in the shop
        
        @rtype: list of Product
        """
        a = []
        d = []
        for i in self.products.all:
            a.append([i.price_ex_vat,i])
        a.sort()
        b = min(a)[0]
        for c in a:
            if c[0] == b:
                d.append(c[1])
        return d
    
    def get_most_expensive_products(self):
        """ Return the most expensive products in the shop
        
        @rtype: list of Product
        """
        a = []
        d = []
        for i in self.products.all:
            a.append([i.price_ex_vat,i])
        a.sort()
        b = max(a)[0]
        for c in a:
            if c[0] == b:
                d.append(c[1])
        return d        
        
    def get_top_products(self, n):        
        """ Return n products that is ordered highest
        
        @param n: number of products
        @type n: integer
        
        @rtype: list of Product
        """        
        b = []
        d = []
        g = []
        h = []
        for i in self.orders.all:
            for x in i.products:
                b.append([x.sku,x])
        b.sort()
        j=1
        e=0
        f=len(b)
        while j< f:
            if b[0][0] == b[1][0]:
                e +=1
                b.remove(b[0])
            else:
                d.append([e+1,b[0][1]])
                e=0
                b.remove(b[0])
            j+=1    
        d.append([e+1,b[0][1]])
        d.sort()
        d.reverse()
        for i in d:
            g.append(i[1])
        for i in range(n):
            h.append(g[i])
        h.sort()
        return h
    