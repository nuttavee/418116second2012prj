# ID:5510450185
# NAME : nuttavee thumniam-in

import math

class RatNum:
    """Represent a rational number.
    
    @ivar nom: the nominator
    @type nom: integer
    @ivar denom: the denominator
    @type denom: integer
    """
    def __init__(self, nom, denom=1):
        """Initialize the nominator and denominator
        with the given value.
        
        @param nom: the nominator
        @type nom: integer
        @param denom: the denominator. Defaulted to 1.
        @type denom: integer
        """
        if denom<0:
            denom*=-1
            nom*=-1
        if denom!=0:
            a=nom
            b=denom
            while b!=0:
               a,b=b,a%b
            self.nom=int(nom/a)
            self.denom=int(denom/a)
        else:
            self.nom=1
            self.denom=0
    @classmethod
    def from_str(klass, s):
        """Convert a string to a rational number.
        
        @param s: string representation of a rational number
        @type s: string
        @rtype: RatNum
        """
        if "/" in s:
            a=s.split("/")
            return RatNum(int(a[0]),int(a[1]))
        elif s=='nan':
            return RatNum(1,0)
        else:
            return RatNum(int(s))
    
    def is_nan(self):
        """Returns True when self is not a number (nan).
        That is, when the denominator is zero.
        
        @rtype: boolean
        """
        if self.denom==0:
            return True
        else:
            return False
    
    def is_positive(self):
        """Check if C{self} is positive.
        
        @rtype: boolean
        """
        if self.nom*self.denom>0:
            return True
        else:
            return False
    
    def is_negative(self):
        """Check if C{self} is negative.
        
        @rtype: boolean
        """
        if self.nom*self.denom<0:
            return True
        else:
            return False    
    def __float__(self):
        """Convert C{self} to a floating point number.
        Must output C{float('nan')} when C{self} is nan.
        
        @rtype: float
        """
        if self.denom==0:
            return float('nan')
        else:
            return float(self.nom)/float(self.denom)
    def __int__(self):
        """Convert C{self} to an integer.
        
            - If C{self} is positive, we round the number up.
            - If C{self} is negative, we round the number down.
        
        @rtype: integer
        """
        x=float(self.nom)/float(self.denom)
        if x>0:
            return int(math.ceil(x))
        else:
            return int(math.floor(x))
    
    def __eq__(self, other):
        """Equality operator.
        
        @param other:
        @type other: C{RatNum} 
        @rtype: boolean
        """
        if self.nom == other.nom and self.denom == other.denom : 
            return True
        else :
            return False
        
        
        
    
    def __ne__(self, other):
        """Inequality operator.
        
        @param other:
        @type other: C{RatNum} 
        @rtype: boolean
        """
        if self.nom != other.nom or self.denom != other.denom:
            return True
        else:
            return False
        
    def __add__(self, other):
        """Addition operator.
        
        @param other:
        @type other: C{RatNum} 
        @rtype: C{RatNum}
        """
        if self.denom == 0:
            return RatNum(1,0)
        else:
            nom=(self.nom*other.denom)+(self.denom*other.nom)
            denom=self.denom*other.denom
            return RatNum(nom,denom)
    
    def __sub__(self, other):
        """Subtraction operator.
        
        @param other:
        @type other: C{RatNum} 
        @rtype: C{RatNum}
        """
        if self.denom == 0:
            return RatNum(1,0)
        else:
            nom=(self.nom*other.denom)-(self.denom*other.nom)
            denom=self.denom*other.denom
            return RatNum(nom,denom)
    def __mul__(self, other):
        """Multiplication operator.
        
        @param other:
        @type other: C{RatNum} 
        @rtype: C{RatNum}
        """
        if self.denom == 0 or other.denom == 0:
            return RatNum(1,0)
        else:
            a=self.nom*other.nom
            b=self.denom*other.denom
            return RatNum(a,b)

    def __div__(self, other):
        """Division operator.
        
        @param other:
        @type other: C{RatNum} 
        @rtype: C{RatNum}
        """
        if self.denom == 0 or other.denom == 0 or other.nom ==0:
            return RatNum(1,0)
        else:
            a=self.nom*other.denom
            b=other.nom*self.denom
            return RatNum(a,b)
    
    def __str__(self):
        """Return the string representation of C{self}
        
            - The string shows the fraction in the
              lowest terms.
        
            - The string will not show the denominator if it is 1.
        
            - Returns C{'nan'} if C{self} is not a number.
        
        @rtype: string
        """
        if self.denom ==0:
            return "nan"
        elif self.denom==1:
            return"{0}".format(self.nom)
        else:
            return "{0}/{1}".format(self.nom,self.denom)
    def __neg__(self):
        """Negation operator.
        
        @rtype: C{RatNum}
        """
        if self.denom == 0 :
            return RatNum(1,0)
        else :
            return RatNum(self.nom*-1,self.denom)
    
    def __cmp__(self, other):
        """Comparision operator.
        
            - Return 1 if C{self} > C{other}.
            - Return -1 if C{self} < C{other}.
            - Return 0 if C{self} = C{other}.
        
        @param other:
        @type other: C{RatNum} 
        @rtype: integer
        """
        if (self.nom*other.denom)>(other.nom*self.denom):
            return 1
        elif (self.nom*other.denom)<(other.nom*self.denom):
            return -1
        else:
            return 0