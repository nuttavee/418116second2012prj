# ID: 5510450185
# NAME: nuttavee thumniam-in

from rat_num import RatNum

class RatTerm:
    """Represent a monomial whose coefficient is a rational number.
    The term has the form C*x^E where C (the coefficient)
    is a rational number, and E (the exponent) is an integer.
    
    @ivar coeff: the coefficient
    @type coeff: C{RatNum}
    @ivar expt: the exponent
    @type expt: integer
    """
    
    def __init__(self, coeff, expt):
        """Initialize the coefficient and the exponent
        with the given values.
        
        @param coeff: the coefficient
        @type coeff: C{RatNum}
        @param expt: the exponent
        @type expt: integer
        """
        self.coeff = coeff
        self.expt = expt
        if coeff==RatNum(0):
            self.expt = 0
    
    def is_nan(self):
        """Check if the term is not a number.
        That is, check if the coefficient is not a number.
        
        @rtype: boolean"""
        if self.coeff==RatNum(1,0):
            return True
        else:
            return False
        
    def is_zero(self):
        """Check if the term is zero.
        That is, check if the coefficient is zero.
        
        @rtype: boolean"""
        if self.coeff==0:
            return True
        else:
            return False
    
    def eval(self, x):
        """Evaluate the monomial at the given x.
        
        @param x:
        @type x: number
        @rtype: number
        """
        return float(self.coeff)*(x**self.expt)
    
    def __eq__(self, other):
        """Equality operator.
        
        @param other:
        @type other: C{RatTerm}
        @rtype: boolean
        """
        if self.coeff == other.coeff and self.expt == other.expt:
            return True
        else:
            return False
    
    def __ne__(self, other):
        """Inequality operator.
        
        @param other:
        @type other: C{RatTerm}
        @rtype: boolean
        """
        if self.coeff != other.coeff or self.expt != other.expt:
            return True
        else:
            return False
    
    @classmethod
    def from_str(klass, s):
        """Convert a string representation of C{RatTerm}
        to a C{RatTerm}
        
        @param s:
        @type s: string
        @rtype: C{RatTerm}
        """
        if "/" in s:
            a=s.find("/")
            nom = s[0:a]
            if "*" in s:
                b=s.find("*")
                denom = s[a+1:b]
                if "^" in s:
                    d = s.find('^')
                    return RatTerm(RatNum(int(nom),int(denom)),int(s[d+1:len(s)]))
                return RatTerm(RatNum(int(nom),int(denom)),1)
            else:
                return RatTerm(RatNum(int(nom),int(s[len(s)-1])),0)
        elif "*" in s:
            b=s.find("*")
            nom = s[0:b]
            if "^" in s:
                c = s.find('^')
                return RatTerm(RatNum(int(nom)),int(s[c+1:len(s)]))
            return RatTerm(RatNum(int(nom)),1)
        elif "^" in s:
            c=s.find("^")
            if '-' == s[0]:
                return RatTerm(RatNum(-1),int(s[c+1:len(s)]))
            else:
                return RatTerm(RatNum(1),int(s[c+1:len(s)]))
        elif 'x' in s:
            a=s.split('x')
            if a[0]=='':
                return RatTerm(RatNum(1),1)
            elif a[0]=='-':
                return RatTerm(RatNum(-1),1)
            else:
                return RatTerm(RatNum(a[0]),1)
        elif s == 'nan':
            return RatTerm(RatNum(1,0),0)
        else:
            return RatTerm(RatNum(int(s)),0)        
    
    @classmethod
    def nan(klass):
        """Return an instance of C{RatTerm} that is not a number.
        
        @rtype: C{RatTerm}
        """
        return RatTerm(RatNum(1,0),0)
    
    @classmethod
    def zero(klass):
        """Return an instance of C{RatTerm} that is equal to zero.
        
        @rtype: C{RatTerm}
        """
        return RatTerm(RatNum(0),0)
    
    def __str__(self):
        """Return a string representation of C{self}.
        
            - The string does not show the exponentiation sign
              and the exponent if the exponent is 1.
            - The string does not show the variable x if
              the exponent is 0.
            - The string does not show the coefficient if the
              exponent is at least one, and the coefficient is
              1 or -1.
            - Return C{'-x'} if the coefficient is -1 and 
              the exponent is 1.
            - Return C{'nan'} if the monomial is not a number.
        
        @rtype: string
        """
        if self.coeff == RatNum(1) and self.expt == 1:
            return 'x'
        elif self.coeff == RatNum(-1) and self.expt == 1:
            return '-x'
        elif self.expt == 1:
            return "{0}*x".format(self.coeff)        
        elif self.expt == 0:
            return "{0}".format(self.coeff)
        elif str(self.coeff) == 'nan':
            return 'nan'        
        elif self.coeff == RatNum(1) and self.expt > 1:
            return "x^{0}".format(self.expt)
        elif self.coeff == RatNum(-1) and self.expt > 1:
            return "-x^{0}".format(self.expt)
        elif self.expt > 1:
            return "{0}*x^{1}".format(self.coeff,self.expt)        
    
    def __add__(self, other):
        """Addition operator.
        
        @raise C{ValueError}: if C{other's} exponent does not 
            equal C{self}'s
        @param other:
        @type other: C{RatTerm}
        @rtype: C{RatTerm}
        """
        if self.coeff == RatNum(1,0) or other.coeff == RatNum(1,0):
            return RatTerm(RatNum(1,0),0)
        elif self.coeff == RatNum(0):
            return other
        elif other.coeff == RatNum(0):
            return self
        elif self.expt != other.expt:
            raise ValueError
        else:
            a =(self.coeff + other.coeff)
            return RatTerm(a,self.expt)        
    
    def __neg__(self):
        """Negation operator.
        
        @rtype: C{RatTerm}
        """
        return RatTerm(self.coeff*RatNum(-1),self.expt)
    
    def __sub__(self, other):
        """Subtraction operator.
        
        @raise C{ValueError}: if C{other's} exponent does not 
            equal C{self}'s
        @param other:
        @type other: C{RatTerm}
        @rtype: C{RatTerm}
        """
        if self.coeff == RatNum(1,0) or other.coeff == RatNum(1,0):
            return RatTerm(RatNum(1,0),0)
        elif self.coeff == RatNum(0):
            return -other
        elif other.coeff == RatNum(0):
            return self
        elif self.expt != other.expt:
            raise ValueError
        else:
            a =(self.coeff - other.coeff)
            return RatTerm(a,self.expt)        

    def __mul__(self, other):
        """Multiplication operator.
        
        @param other:
        @type other: C{RatTerm}
        @rtype: C{RatTerm}
        """
        if self.coeff == RatNum(1,0) or other.coeff == RatNum(1,0):
            return RatTerm(RatNum(1,0),0)      
        else:
            a=(self.coeff*other.coeff)
            b=(self.expt+other.expt)
            return RatTerm(a,b)        
    
    def __div__(self, other):
        """Division operator.
        
        @param other:
        @type other: C{RatTerm}
        @rtype: C{RatTerm}
        """
        if self.coeff == RatNum(1,0) or other.coeff == RatNum(1,0):
            return RatTerm(RatNum(1,0),0)
        elif other.coeff == RatNum(0):
            return RatTerm(RatNum(1,0),0)
        else:
            a=(self.coeff/other.coeff)
            b=(self.expt-other.expt)
            return RatTerm(a,b)        
    
    def differentiate(self):
        """Return the derivative of C{self}.
        
        Recall that the derivative of C*x^E is (C*E)*x^(E-1).
        
        @rtype: C{RatTerm}
        """        
        if self.coeff == RatNum(1,0):
            return RatTerm(RatNum(1,0),0)
        else:
            a = (self.coeff*RatNum(self.expt))
            b = (self.expt-1)
            return RatTerm(a,b)
    
    def anti_differentiate(self):
        """Return an anti-derivative of C{self} with constant 0.
        
        Recall that the anti-derivative of C*x^E is C/(E+1)*x^(E+1) + c,
        where c is a constant. Here, we use c = 0 in our output.
        
        @rtype: C{RatTerm}
        """
        if self.coeff == RatNum(1,0):
            return RatTerm(RatNum(1,0),0)              
        else:
            a = (self.coeff/RatNum(self.expt+1))
            b = (self.expt+1)
            return RatTerm(a,b)        